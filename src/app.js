require('dotenv').config()
const express = require('express')
const path = require('path')
const app = express();
const hbs = require('hbs')
require("./db/conn")   /// database connection is done by including db file
const auth = require('./middleware/auth')
const Register = require("./models/registers")
const bcrypt = require('bcryptjs')

const cookieParser = require('cookie-parser')

const port = process.env.port || 8000   // it will enerate the port number

const static_path = path.join(__dirname, "../public")
const template_path = path.join(__dirname, "../templates/views")
const partials_path = path.join(__dirname, "../templates/partials")
//console.log(path.join(__dirname,"../public"))

app.use(express.json())
app.use(cookieParser())
app.use(express.urlencoded({ extended: false })) //// the data which we required is getting
//app.use(cookieParser)

app.use(express.static(static_path))

app.set("view engine", "hbs")   // using views

app.set("views", template_path)
hbs.registerPartials(partials_path)

/////console.log(process.env.SECRET_KEY)

app.get("/", (req, res) => {   /////////////by default url ---- display the content by default
    res.render("index") ///// rendering the index page
})
///// use auth function
app.get("/secret",auth, (req, res) => {
    //console.log(`this is the cookie awesome ${req.cookies.jwt}`)
    res.render("secret");
})

app.get("/logout",auth, async (req,res) =>{
    try {
        req.user.tokens= req.user.tokens.filter((currentElement) =>{
            return currentElement.token != req.token
        })

        res.clearCookie("jwt")
        console.log("logout successfully")
        await req.user.save()
        res.render("login")

    } catch (error) {
        res.send(500).send(error)
    }
})

app.get("/register", (req, res) => {
    res.render("register");
})


app.get("/login", (req, res) => {
    res.render("login")
})
////// creating user 
app.post("/register", async (req, res) => {
    try {
        //console.log(req.body.firstname);
        //res.send(req.body.firstname)
        const password = req.body.password
        const confirmpassword = req.body.confirmpassword

        if (password === confirmpassword) {
            const registerEmp = new Register({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                gender: req.body.gender,
                phone: req.body.phone,
                password: password,
                confirmpassword: confirmpassword
            })

            console.log("success part "+registerEmp)
            const token = await registerEmp.generateAuthToken();
            console.log("the token part "+token)  
            
                res.cookie("jwt",token,{
                expires :new Date(Date.now() + 30000),
                httpOnly :true
            })           
          
            res.render("index");
        } else {
            res.send("password does not match....")
        }

    }
    catch (error) {
        res.status(400).send(error);
    }
})

////login post method------------------return the confirmation in callback if timeset is more then it will await that function till receiving the response

app.post("/login", async (req, res) => {
    try {
        const email = req.body.email;
        const password = req.body.password;
        const useremail = await Register.findOne({ email: email })
        const isMatch = await bcrypt.compare(password, useremail.password)

        const token = await useremail.generateAuthToken();
        console.log("the token part "+token) 

        res.cookie("jwt",token,{
            expires :new Date(Date.now() + 30000),
            httpOnly :true
        })
        //////console.log(`this is the cookie awesome ${req.cookies.jwt}`)

        if (isMatch,callbackify) {
            res.status(201).render("index")
        } else {
            res.send("Invalid login details")
        }

        // console.log(email)
    } catch (error) {
        res.status(400).send("Invalid login details")
    }
})

//token generation
const jwt = require('jsonwebtoken');
const { callbackify } = require('util');
const createToken = async () => {
    const token = await jwt.sign({ _id: "6057bd8870c8102d147287dd" }, "my name is yogita", { expiresIn: "2 seconds" })   //secrete key should be 32 bit longer
    console.log(token)
    const userVer = await jwt.verify(token, "my name is yogita")
    console.log(userVer)
}

createToken();

app.listen(port, () => {
    console.log(`server is running at the port no ${port}`)
})