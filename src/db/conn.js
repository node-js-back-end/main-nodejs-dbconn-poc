const mongoose = require('mongoose')
mongoose.connect("mongodb://localhost:27017/youtbregister", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(() => {
    console.log('connection successful')
}).catch(() => {
    console.log('connection failed')
})
/////database connection returns promise whether it will connect or reject the connection.
function asyncDBconnect(param) {

    return new Promise((req, res) => {

        mongoose.connect(param, (err, connection) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(connection);
            }
        });

    });
}



